#!/usr/bin/env bash
# backup workspace to NFS share
SRC="/var/lib/jenkins/"
SYNC="/mnt/bckup/jenkins"
DEST="/mnt/bckup/jenkins_bckup/"
FILE="jenkins-$(date +%Y-%m-%d).tar.gz"

# Ensure directories exist
#mkdir -p $DEST
#mkdir -p $SYNC

# Record start time by epoch second
START=$(date '+%s')

if ! rsync -av --delete $SRC $SYNC ; then
  STATUS="rsync failed"
elif ! tar -czf $FILE $SYNC ; then
  STATUS="tar failed"
elif ! mv $FILE $DEST ; then
  STATUS="mv failed"
else
  STATUS="success: size=$(stat -c%s $DEST$FILE) duration=$((`date '+%s'` - $START))"
fi

# Log to system log; handle this using syslog(8)
logger -t backup "$STATUS"
echo "$STATUS"
